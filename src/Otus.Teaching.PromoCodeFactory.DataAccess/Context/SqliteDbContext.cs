﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
	public class SqliteDbContext : DbContext
	{
		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Preference> Preferences { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<PromoCode> PromoCodes { get; set; }

		public SqliteDbContext(DbContextOptions<SqliteDbContext> options) : base(options)
		{

		}

		public void ReCreateDb()
		{
			Database.EnsureDeleted();
			Database.EnsureCreated();
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseLazyLoadingProxies();
			base.OnConfiguring(optionsBuilder);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Role>(RoleBuildAction);
			modelBuilder.Entity<Employee>(EmployeeBuildAction);
			modelBuilder.Entity<Preference>(PreferenceBuildAction);
			modelBuilder.Entity<Customer>(CustomerBuildAction);
			modelBuilder.Entity<CustomerPreference>(CustomerPreferenceBuildAction);
			modelBuilder.Entity<PromoCode>(PromoCodeBuildAction);

			base.OnModelCreating(modelBuilder);

			//  Seed
			modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
			modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
			modelBuilder.Entity<CustomerPreference>().HasData(new CustomerPreference() { Id = Guid.NewGuid(), CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") });
			modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
			modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
			modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);
		}

		public void RoleBuildAction(EntityTypeBuilder<Role> builder)
		{
			builder.Property(p => p.Name).IsRequired().HasMaxLength(10);
			builder.Property(p => p.Description).HasMaxLength(100);
		}

		public void EmployeeBuildAction(EntityTypeBuilder<Employee> builder)
		{
			builder.HasOne(e => e.Role).WithMany(r => r.Employees).HasForeignKey(e => e.RoleId);
			builder.Property(p => p.FirstName).IsRequired().HasMaxLength(50);
			builder.Property(p => p.LastName).IsRequired().HasMaxLength(50);
			builder.Property(p => p.Email).HasMaxLength(50);
		}

		public void PreferenceBuildAction(EntityTypeBuilder<Preference> builder)
		{
			builder.Property(p => p.Name).HasMaxLength(10);
		}

		public void CustomerBuildAction(EntityTypeBuilder<Customer> builder)
		{
			builder.Property(p => p.FirstName).IsRequired().HasMaxLength(50);
			builder.Property(p => p.LastName).IsRequired().HasMaxLength(50);
			builder.Property(p => p.Email).HasMaxLength(50);
		}

		public void CustomerPreferenceBuildAction(EntityTypeBuilder<CustomerPreference> builder)
		{
			builder.HasOne(cp => cp.Customer).WithMany(c => c.CustomerPreferences).HasForeignKey(cp => cp.CustomerId);
			builder.HasOne(cp => cp.Preference).WithMany(c => c.CustomerPreferences).HasForeignKey(cp => cp.PreferenceId);
		}

		public void PromoCodeBuildAction(EntityTypeBuilder<PromoCode> builder)
		{
			builder.Property(p => p.Code).IsRequired().HasMaxLength(10);
			builder.Property(p => p.ServiceInfo).HasMaxLength(100);
			builder.Property(p => p.PartnerName).HasMaxLength(50);
			builder.HasOne(pc => pc.Preference).WithMany(p => p.PromoCodes).HasForeignKey(pc => pc.PreferenceId);
			builder.HasOne(pc => pc.Customer).WithMany(c => c.PromoCodes).HasForeignKey(pc => pc.CustomerId);
		}


	}
}
