﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class EfRepository<T> : IRepository<T> where T : BaseEntity
	{
		private DbContext _dbContext;

		public EfRepository(SqliteDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public Task<IEnumerable<T>> GetAllAsync()
		{
			return Task.FromResult(_dbContext.Set<T>().AsEnumerable<T>());
		}

		public virtual Task<T> GetByIdAsync(Guid id)
		{
			return Task.FromResult(_dbContext.Set<T>().FirstOrDefault<T>(x => x.Id == id));
		}

		public async Task<T> CreateAsync(T entity)
		{
			_dbContext.Set<T>().AddAsync(entity);
			_dbContext.SaveChangesAsync();
			return await GetByIdAsync(entity.Id);
		}

		public async Task<int> DeleteAsync(T entity)
		{
			_dbContext.Set<T>().Remove(entity);
			return await _dbContext.SaveChangesAsync();
		}

		public async Task<T> UpdateAsync(T entity)
		{
			_dbContext.Set<T>().Update(entity);
			_dbContext.SaveChangesAsync();
			return await GetByIdAsync(entity.Id);
		}

	}
}