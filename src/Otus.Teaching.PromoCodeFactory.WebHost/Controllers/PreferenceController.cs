﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
	[Route("api/v1/[controller]")]
	[ApiController]
	public class PreferenceController : ControllerBase
	{
		private readonly IRepository<Preference> _preferenceRepository;
		public PreferenceController(IRepository<Preference> preferenceRepository)
		{
			_preferenceRepository = preferenceRepository;
		}

		/// <summary>
		/// Получить список всех предпочтений
		/// </summary>
		/// <returns></returns>
		public async Task<List<PreferenceResponse>> GetResponsesAsync()
		{
			var preferences = await _preferenceRepository.GetAllAsync();

			var preferencesModelList = preferences.Select(x =>
				new PreferenceResponse()
				{
					Id = x.Id,
					Name = x.Name
				}).ToList();

			return preferencesModelList;
		}

	}
}
